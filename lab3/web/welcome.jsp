<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: asushchyk
  Date: 26.11.17
  Time: 23:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome!</title>
    <link href="css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include  file="menu.html" %>
<%
    Connection conn = null;
    String Message = "Welcome, " + request.getParameter("username") + "!";
    try {

        String username=request.getParameter("username");
        String password=request.getParameter("password");
        String fullName=request.getParameter("full_name");
        String email=request.getParameter("email");

        String dbURL = "jdbc:mysql://localhost:3306/internet_lab2";
        String user = "root";
        String pwd = "root";
        String driver = "com.mysql.jdbc.Driver";

        String query = "INSERT INTO users (username, password, full_name, email) " +
                "VALUES ('"+username+"','"+password+"','"+fullName+"','"+email+"')";
        Class.forName(driver);
        conn = DriverManager.getConnection(dbURL, user, pwd);
        Statement statement = conn.createStatement();
        statement.executeUpdate(query);
        statement.close();

    } catch( SQLException e ){
        Message = "User with that username already in database.";
    }
%>
<h2 style="text-align: center"><%= Message %></h2>
</body>
</html>
