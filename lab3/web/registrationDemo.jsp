<%--
  Created by IntelliJ IDEA.
  User: asushchyk
  Date: 26.11.17
  Time: 23:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Registration Demo</title>
        <link href="css/app.css" rel="stylesheet" type="text/css">
    </head>
<body>
<%@include  file="menu.html" %>
    <div>
        <form action="welcome.jsp" method="POST">
            Full name:<br>
            <input type="text" name="full_name" required>
            <br>
            User name:<br>
            <input type="text" name="username" required><br>
            Email:<br>
            <input type="email" name="email" required><br>
            Password:<br>
            <input type="password" name="password" required><br>
            <button type="submit">Submit</button>
        </form>
    </div>
</body>
</html>
