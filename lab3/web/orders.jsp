<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Orders</title>
    <link href="css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include  file="menu.html" %>
<sql:setDataSource
        var="myDS"
        driver="com.mysql.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/internet_lab2"
        user="root" password="root"
/>
<sql:query var="list_product_types" dataSource="${myDS}">
    SELECT id, customer_name, customer_phone_number, DATE_FORMAT(created_at, "%M %d %Y %k:%i") AS datetime, (SELECT title FROM products WHERE id = orders.product_id) AS prod_title FROM orders  ORDER BY created_at DESC;</sql:query>

<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>Orders</h2></caption>
        <tr>
            <th>ID</th>
            <th>Product</th>
            <th>Order's datetime</th>
            <th>Customer name</th>
            <th>Customer phone number</th>
        </tr>
        <c:forEach var="productType" items="${list_product_types.rows}">
            <tr>
                <td><c:out value="${productType.id}" /></td>
                <td><c:out value="${productType.prod_title}" /></td>
                <td><c:out value="${productType.datetime}" /></td>
                <td><c:out value="${productType.customer_name}" /></td>
                <td><c:out value="${productType.customer_phone_number}"/></td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
