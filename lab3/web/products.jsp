<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product Types</title>
    <link href="css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<%@include  file="menu.html" %>
<sql:setDataSource
        var="myDS"
        driver="com.mysql.jdbc.Driver"
        url="jdbc:mysql://localhost:3306/internet_lab2"
        user="root" password="root"
/>
<sql:query var="list_product_types" dataSource="${myDS}">
    SELECT products.id, products.title, price, is_available,
    (SELECT title FROM product_types WHERE id = products.product_type_id) AS prod_type_title
    FROM products
    ORDER BY title ASC;
</sql:query>

<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>Products</h2></caption>
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Price</th>
            <th>Category</th>
            <th>Is available</th>
        </tr>
        <c:forEach var="productType" items="${list_product_types.rows}">
            <tr>
                <td><c:out value="${productType.id}" /></td>
                <td><c:out value="${productType.title}" /></td>
                <td><c:out value="${productType.price}"/></td>
                <td><c:out value="${productType.prod_type_title}"/></td>
                <td><c:out value="${not empty productType.is_available ? 'Yes' : 'No'}"/></td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
