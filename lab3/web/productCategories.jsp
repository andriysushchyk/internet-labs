<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product Categories</title>
    <link href="css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
    <%@include  file="menu.html" %>
    <sql:setDataSource
            var="myDS"
            driver="com.mysql.jdbc.Driver"
            url="jdbc:mysql://localhost:3306/internet_lab2"
            user="root" password="root"
    />
    <sql:query var="list_product_types" dataSource="${myDS}">
        SELECT product_types.*, (SELECT COUNT(*) FROM products WHERE product_type_id = product_types.id) AS products_count FROM product_types
    </sql:query>

    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>Product Types</h2></caption>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Products Count</th>
            </tr>
            <c:forEach var="productType" items="${list_product_types.rows}">
                <tr>
                    <td><c:out value="${productType.id}" /></td>
                    <td><c:out value="${productType.title}" /></td>
                    <td><c:out value="${productType.products_count}"/></td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
