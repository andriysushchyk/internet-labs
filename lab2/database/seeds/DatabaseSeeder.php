<?php

use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Order::query()->delete();
        Product::query()->delete();
        ProductType::query()->delete();
        $categories = [
            'Phones',
            'Computers and laptops',
            'Software',
        ];

        foreach ($categories as $category) {
            ProductType::create(['title' => $category]);
        }

        $this->addPhoneProducts();
        $this->addLabtopProducts();
        $this->addSoftwareProducts();
        $this->addOrders();
    }

    public function addPhoneProducts() {
        $categoryID = ProductType::where('title', 'Phones')->first()->id;
        Product::create([
                'title' => 'IPhone 6s',
                'product_type_id' => $categoryID,
                'price' => 13000
        ]);

        Product::create([
                'title' => 'IPhone X',
                'product_type_id' => $categoryID,
                'price' => 40000
        ]);

        Product::create([
                'title' => 'Huawei Y6 Pro',
                'product_type_id' => $categoryID,
                'price' => 4500.40
        ]);
    }

    public function addLabtopProducts() {
        $categoryID = ProductType::where('title',  'Computers and laptops')->first()->id;

        Product::create([
            'title' => 'Asus X55VD (X55VD-SX003D) Dark Blue',
            'product_type_id' => $categoryID,
            'price' => 15233.00
        ]);

        Product::create([
                'title' => 'Dell Vostro 15 3568',
                'product_type_id' => $categoryID,
                'price' => 15500,
                'is_available' => 0
        ]);

        Product::create([
                'title' => 'Dell Vostro 13 5568',
                'product_type_id' => $categoryID,
                'price' => 25000
        ]);
    }

    public function addSoftwareProducts() {
        $categoryID = ProductType::where('title', 'Software')->first()->id;
        Product::create([
                'title' => 'Microsoft Office 2016',
                'product_type_id' => $categoryID,
                'price' => 10000
        ]);

        Product::create([
                'title' => 'Office 365',
                'product_type_id' => $categoryID,
                'price' => 1444.10
        ]);

        Product::create([
                'title' => 'Kasperky antivirus',
                'product_type_id' => $categoryID,
                'price' => 2342.40,
                'is_available' => 0
        ]);
    }


    public function addOrders() {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 30; $i++) {
            \App\Models\Order::create([
                'product_id' => Product::query()->orderByRaw("RAND()")->first()->id,
                'customer_name' => $faker->firstName . " " . $faker->lastName,
                'customer_phone_number' => $faker->e164PhoneNumber,
                'created_at' => $faker->dateTimeBetween('-1 month', 'now')
            ]);
        }
    }
}
