<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function showRegistrationForm() {

        if (Auth::check()) {
            return redirect()->to('/welcome');
        }

        return view('registration.form');
    }

    public function register(Request $request) {
        $userData = $this->validate($request, [
            'username' => "required|unique:users",
            'password' => 'required',
            'full_name' => 'required',
            'email' => 'required|email'
        ]);

        $userData['password'] = bcrypt($userData['password']);
        $user = User::create($userData);
        Auth::login($user);
        return redirect()->to('welcome');
    }

    public function welcome() {

        if (!Auth::check()) {
            return redirect()->to('register');
        }

        return view('registration.welcome');
    }

    public function logout() {
        Auth::logout();
        return redirect()->to('register');
    }
}
