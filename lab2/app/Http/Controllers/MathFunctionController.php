<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MathFunctionController extends Controller
{
    public function show() {
        return view("math_function.index", [
                'x' => null,
                'y' => null,
        ]);
    }

    public function solve(Request $request)
    {
        $this->validate($request, [
                'x' => 'required|numeric'
        ]);

        $x = $request->input("x");
        $y = 234;
        return view("math_function.index", [
                'x' => $x,
                'y' => $y
        ]);
    }
}
