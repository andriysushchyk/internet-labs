@extends('partials.base')
@section('content')
    <div class="container">
        <div class="row">
            <h2 class="text-center">Function solver</h2>
            <div class="col-md-4 col-md-offset-4">
                <div class="row text-center">
                <img style="width: 100%" src="{{ asset("equal.png") }}">
                </div>
                <div class="row text-center">
                    @if(null !== $x && null !== $y)
                        When x = {{ round($x, 2) }}, y = {{ round($y, 2) }}
                    @endif
                </div>
                <div class="row text-center">
                    @if($errors->all())
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-warning">{{ $error }}</div>
                        @endforeach
                    @endif
                </div>
                <div class="row text-center">
                <form action="{{ url("math_function") }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="email">Enter 'x' value:</label>
                        <input type="text" name="x" class="form-control" id="email" value="{{ $x }}">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-default">Calculate</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
@endsection