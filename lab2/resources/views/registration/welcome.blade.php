@extends('partials.base')
@section('content')
<div class="container">
    <div class="col-md-12 text-center">
        <h2>Welcome, {{ \Illuminate\Support\Facades\Auth::user()->full_name }}!</h2>
        <form action="{{ url('logout') }}">
        <button class="btn btn-warning">Log out</button>
        </form>
    </div>
</div>
@endsection