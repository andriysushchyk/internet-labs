@extends('partials.base')
@section('content')
<div class="container">
    <div class="col-md-offset-2 col-md-8">
        <form class="form-horizontal" role="form" action="{{ '/register' }}" method="POST">
            {{ csrf_field() }}
            <h2 class="text-center">Sign up</h2>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group">
                <label for="fullName" class="col-sm-2 control-label">Full Name</label>
                <div class="col-sm-10">
                    <input type="text" name="full_name" id="fullName" placeholder="Full Name" class="form-control" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="userName" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                    <input name="username" type="text" id="userName" placeholder="Username" class="form-control" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input name="email" type="email" id="email" placeholder="Email" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                    <input name="password" type="password" id="password" placeholder="Password" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Register</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection