@extends('partials.base')
@section('content')
    <div class="container">
        <div class="col-md-offset-1 col-md-10">
            <h1 class="text-center">Orders</h1>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Product</th>
                    <th class="text-center">Customer's name</th>
                    <th class="text-center">Customer's` phone number</th>
                    <th class="text-center">Order time</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td class="text-center">{{ $order->product->title }}</td>
                        <td class="text-center">{{ $order->customer_name }}</td>
                        <td class="text-center">{{ $order->customer_phone_number }}</td>
                        <td class="text-center" title="{{  $order->created_at }}">{{ $order->created_at->diffForHumans() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection