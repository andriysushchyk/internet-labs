@extends('partials.base')
@section('content')
    <div class="container">
        <div class="col-md-offset-2 col-md-8">
            <h1 class="text-center">Product Categories</h1>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Products count</th>
                </tr>
                </thead>
                <tbody>
                @foreach($productTypes as $productType)
                    <tr>
                        <td class="text-center">{{ $loop->iteration }}</td>
                        <td class="text-center">{{ $productType->title }}</td>
                        <td class="text-center">{{ $productType->products->count() }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection