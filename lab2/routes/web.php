<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('product_types', 'ProductTypeController@index');
Route::get('products', 'ProductController@index');
Route::get('orders', 'OrderController@index');

Route::get('welcome', 'RegistrationController@welcome');
Route::get('register', 'RegistrationController@showRegistrationForm');
Route::post('register', 'RegistrationController@register');
Route::get('logout', 'RegistrationController@logout');

Route::get('math_function', 'MathFunctionController@show');
Route::post('math_function', 'MathFunctionController@solve');